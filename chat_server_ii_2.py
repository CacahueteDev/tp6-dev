import asyncio
from asyncio import StreamReader, StreamWriter


async def amain():
    server = await asyncio.start_server(on_user_connected, port=7777)

    async with server:
        await server.serve_forever()


async def on_user_connected(reader: StreamReader, writer: StreamWriter):
    addr = writer.get_extra_info('peername')
    writer.write(f'Hello {addr}\n'.encode('utf-8'))
    await writer.drain()
    while True:
        line = await reader.readline()

        message = line.decode()
        print(message)


asyncio.run(amain())
