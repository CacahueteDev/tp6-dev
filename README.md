# TP6

## I. Asynchrone

### Mesure !

```shell
$ time python .\web_async_multiple.py .\links
D:\Repos\tp6-dev\web_async_multiple.py:31: DeprecationWarning: There is no current event loop
  loop = asyncio.get_event_loop()
0.00user 0.00system 0:00.70elapsed 0%CPU (0avgtext+0avgdata 5980maxresident)k
0inputs+0outputs (1549major+0minor)pagefaults 0swaps
$ time python .\web_sync_multiple.py .\links 
0.00user 0.00system 0:01.58elapsed 0%CPU (0avgtext+0avgdata 5976maxresident)k
0inputs+0outputs (1549major+0minor)pagefaults 0swaps
```

| Synchrone | Asynchrone |
|-----------|------------|
| 1.58s     | 0.70s      |

