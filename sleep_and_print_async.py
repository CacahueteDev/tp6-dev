import time
import asyncio


async def count_to_ten():
    for i in range(0, 10):
        print(i)
        await asyncio.sleep(0.5)


loop = asyncio.get_event_loop()

tasks = [
    loop.create_task(count_to_ten()),
    loop.create_task(count_to_ten()),
]

loop.run_until_complete(asyncio.wait(tasks))
loop.close()
