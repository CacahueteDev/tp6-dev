from socket import socket, AF_INET, SOCK_STREAM

host = '127.0.0.1'  # IP du serveur
port = 7777  # Port choisir par le serveur

s = socket(AF_INET, SOCK_STREAM)
s.connect((host, port))
file = s.makefile('rw')
print(f'Connected to {host}:{port}')

while True:
    line = input('>')

    file.write(f'{line}\n')
    file.flush()

    print(file.readline())
