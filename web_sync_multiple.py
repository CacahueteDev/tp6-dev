import asyncio
import sys

import requests


def get_content(url: str) -> bytes:
    resp = requests.get(url=url)

    if resp.status_code != 200:
        print(f'GET {url}: {resp.status_code}')

    return resp.content


def write_content(content: bytes, file: str):
    f = open(file=file, mode='wb')
    f.write(content)
    f.close()


if __name__ == '__main__':
    filename = sys.argv[1]
    fileLines = open(filename, 'r').readlines()

    for url in fileLines:
        url = url.strip('\n')
        write_content(get_content(url), f'web_{url.replace('https://', '').strip()}.html')

