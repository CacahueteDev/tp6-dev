import sys

import requests


def get_content(url: str) -> bytes:
    resp = requests.get(url=url)

    if resp.status_code != 200:
        print(f'GET {url}: {resp.status_code}')

    return resp.content


def write_content(content: bytes, file: str):
    f = open(file=file, mode='wb')
    f.write(content)
    f.close()


if __name__ == '__main__':
    url = sys.argv[1]

    write_content(get_content(url), 'web_page.html')