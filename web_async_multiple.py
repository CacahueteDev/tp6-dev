import asyncio
import sys

import aiofiles
import aiohttp
import requests


async def get_content(url: str) -> bytes:
    async with aiohttp.ClientSession() as session:
        resp = await session.get(url=url)

        if resp.status != 200:
            print(f'GET {url}: {resp.status}')

        return await resp.content.read()


async def write_content(content: bytes, file: str):
    async with aiofiles.open(file, mode='wb') as f:
        await f.write(content)


async def run(url: str):
    await write_content(await get_content(url), f'web_{url.replace('https://', '').strip()}.html')


if __name__ == '__main__':
    filename = sys.argv[1]
    fileLines = open(filename, 'r').readlines()
    loop = asyncio.get_event_loop()
    tasks = []

    for url in fileLines:
        tasks.append(loop.create_task(run(url.strip('\n'))))

    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()

