import sys
import aiohttp, aiofiles, asyncio
from aiohttp import ClientSession


async def get_content(url: str, session: ClientSession) -> bytes:
    resp = await session.get(url=url)

    if resp.status != 200:
        print(f'GET {url}: {resp.status}')

    return await resp.content.read()


async def write_content(content: bytes, file: str):
    async with aiofiles.open(file, mode='wb') as f:
        await f.write(content)


async def run():
    async with aiohttp.ClientSession() as session:
        await write_content(await get_content(url, session), '/tmp/web_page')


url = sys.argv[1]
asyncio.run(run())
